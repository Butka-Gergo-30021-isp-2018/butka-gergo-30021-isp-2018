public class VideocardModel extends Videocard {
    String type;
    //public VideocardModel(){
        //type="Nvidia";
   // }
    public VideocardModel(String name,String type){
        super(name);
        this.type=type;
    }
    public String getType(){
        return type;
    }
    public String toString(){
        return "the videocard "+super.toString()+" and its type is : "+getType();
    }
}
