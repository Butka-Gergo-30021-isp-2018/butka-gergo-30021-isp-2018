public class Videocard {
    String name;
    public Videocard(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public String toString(){
        return "and its name is: "+getName();
    }

    public static void main(String[] args) {
        VideocardModel v=new VideocardModel("AMD R 280","something");
        System.out.println(v.toString());
    }
}
