package e30021.Sipos.Tamas.l5.ex2;

public class BiirdControler {
    Biird[] birds = new Biird[2];
    BiirdControler(){
        birds[0] = createBird();
        birds[1] = createBird();
    }
    public void relocateBirds(){
        for(int i=0;i<birds.length;i++)
            birds[i].move();
    }

    private Biird createBird(){
        int i = (int)(Math.random()*10);
        if(i<5)
            return new Penguin();
        else
            return new Goose();
    }

    public static void main(String [] args){
        BiirdControler bc = new BiirdControler();
        bc.relocateBirds();
    }
}
