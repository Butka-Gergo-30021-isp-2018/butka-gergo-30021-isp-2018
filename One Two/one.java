package e30021.Sipos.Tamas.l5.ex4;

public class one {
    int value;
    one(int value){
        this.value=value;
    }
    public int Thevalue(){
        Thevallue();
        value+=3;
        System.out.println("the value is: "+value);
        return value;
    }
    public int Thevallue(){
        value+=1;
        System.out.println("theeee value "+value);
        return value;
    }


    public static void main(String[] args) {
        one t=new one(5);
        System.out.println(t.Thevalue());
        Two l = new Two(5);
        System.out.println(l.Thevallue());
        one m=new one(8);
        int asd=m.Thevalue();
        System.out.println(asd);

        //In clasa derivata am suprascris a doua metoda si cand creeam un obiect pentru subclasa noastra atunci metoda
        //care este suprascris in subclasa va rula Am dat value=5 in subclass am suprascris metoda Thevallue().
        // in locul de value+=1; avem value+=2; Si va rula acea comanda deci in final value=5+2=7

        //Prin upcasting apelem superclassul va rula prima metoda unde se afla al doilea metoda cand m e 8 atunci
        //Prima data va rula Thevallue() care incrementeaza value cu 1 iar apoi va rula prima metoda Thevalue()
        //care dupa aia va incrementa atribuitul value dar care acum are valoarea 9 si in final va fi
        // value=9+3=12
    }
}
