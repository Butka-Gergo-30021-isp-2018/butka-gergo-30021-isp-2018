import java.util.Scanner;
import java.util.Random;
public class ex7 {
    public static void main(String[] args) {
            Random random = new Random();
            Scanner input = new Scanner(System.in);
            int attemptnum = 0;
            int maxattempt = 3;
            int num = random.nextInt(99)+1;
            int number;
        do {
                System.out.print("Ghiceste numarul: ");
                number = input.nextInt();

                if (number > num)
                    System.out.println("Raspuns gresit, numarul tau este prea mare " + number);
                else if (number < num)
                    System.out.println("Raspuns gresit, numarul tau este prea mic " + number);
                else
                    System.out.println("Correct! " + number + " Acesta a fost numarul! " );
            } while (number != num && ++attemptnum<maxattempt);
            if (attemptnum==maxattempt)
                System.out.println("Ai pierdut numarul a fost:"+num);
        }
    }
