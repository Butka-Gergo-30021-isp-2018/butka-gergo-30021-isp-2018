public class Controller {
    private String location;
    private Sensor tempSensor;

    public Controller(String location, Sensor tempSensor) {
        this.location = location;
        this.tempSensor = tempSensor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Sensor getTempSensor() {
        return tempSensor;
    }

    public void setTempSensor(Sensor tempSensor) {
        this.tempSensor = tempSensor;
    }
    public void checkTemperature(){
        System.out.println("the temp is "+tempSensor);
    }

    @Override
    public String toString() {
        return " " +
                "location=" + location +
                ", tempSensor=" + tempSensor;
    }

    public static void main(String[] args) {
        Sensor s=new Sensor(3,"asd");
        System.out.println(s.toString());
        Controller r=new Controller("free",s);
        System.out.println(r.toString());
        r.checkTemperature();

    }
}
