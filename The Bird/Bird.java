package e30021.Sipos.Tamas.l5.ex1;

class Bird {
    public void move(){
        System.out.println("The bird is moving.");
    }
}

class Penguin extends Bird{
    public void move(){
        System.out.println("The PENGUIN is swiming.");
    }
}

class Goose extends Bird{
    public void move(){
        System.out.println("The GOOSE is flying.");
    }
}
class Turtle extends Bird{
    public void move(){
        System.out.println("The turtle is moving slowly.");
    }
}
