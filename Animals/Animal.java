package e30021.Sipos.Tamas.l5.ex3;

import e30021.Sipos.Tamas.l5.ex3.Birdii;

public interface Animal {
    void move();
    void relocateBirds();
    Birdii createBird();
}
