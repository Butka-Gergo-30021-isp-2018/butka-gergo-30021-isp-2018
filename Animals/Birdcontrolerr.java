package e30021.Sipos.Tamas.l5.ex3;

public class Birdcontrolerr implements Animal {
    Birdii[] birdii = new Birdii[3];
    Birdcontrolerr(){
        birdii[0] = createBird();
        birdii[1] = createBird();
        birdii[2] = createBird();
    }

    @Override
    public void move() {

    }

    public void relocateBirds(){
        for(int i=0;i<birdii.length;i++)
            birdii[i].move();
    }

     public Birdii createBird(){
        int i = (int)(Math.random()*10);
        if(i<5)
           return new Goose();
       else if (i>6)
           return new Penguin();
       else
           return new Birdii();
   }

    public static void main(String [] args){
        Birdcontrolerr bc = new Birdcontrolerr();
        bc.relocateBirds();

    }
}
